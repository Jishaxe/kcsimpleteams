package co.uk.joshleeprogrammer.joshlee;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class KCSimpleTeams extends JavaPlugin implements Listener{
	public String TeamAName = "Red";
	public String TeamBName = "Blue";

	public String TeamAPrefix = ChatColor.RED + "[R]";
	public String TeamBPrefix = ChatColor.BLUE + "[B]";

	public List<String> TeamAPlrs;
	public List<String> TeamBPlrs;
	public List<String> NeutralPlrs;


	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(this, this);
		TeamAPlrs = new ArrayList<String>();
		TeamBPlrs = new ArrayList<String>();
		NeutralPlrs = new ArrayList<String>();

		getLogger().info("Reading data...");
		getLogger().info("Reading config file: plugins/KCSimpleTeams/config.dat");
		try
		{
			File dir = new File("plugins/KCSimpleTeams");
			if (!dir.exists())
			{
				getLogger().info("Creating directory KCSimpleTeams in plugins...");
				dir.mkdirs();
			}

			File f = new File("plugins/KCSimpleTeams/config.dat");
			if (!f.exists())
			{
				getLogger().info("A new configuration file, plugins/KCSimpleTeams/config.dat has been created. Default values have been added. Values are listed like this:");
				getLogger().info("\n<TeamAName>\n<TeamBName>\n<TeamAPrefix>\n<TeamBPrefix>");
				f.createNewFile(); 
				// Write defaults
				try
				{
					BufferedWriter tmpout = new BufferedWriter(new FileWriter("plugins/KCSimpleTeams/config.dat",true));
					tmpout.write("Red\r\n");
					tmpout.write("Blue\r\n");
					tmpout.write("&4[R]&f\r\n");
					tmpout.write("&1[B]&f");
					tmpout.close();
				}
				catch (Exception e)
				{
					getLogger().info("Config file error: " + e.getMessage());
				}
			}

			File file = new File("plugins/KCSimpleTeams/config.dat"); 
			Scanner scanner = null;
			String strLine;
			int linecount = 0;

			try 
			{
				scanner = new Scanner(file);

				while (scanner.hasNextLine())
				{
					linecount++;
					strLine = scanner.nextLine();

					if (linecount == 1)
						TeamAName = strLine;
					if (linecount == 2)
						TeamBName = strLine;
					if (linecount == 3)
						TeamAPrefix = strLine;
					if (linecount == 4)
						TeamBPrefix = strLine;
				}
			}
			catch (Exception e)
			{
				getLogger().info("Config file error: " + e.getMessage());
			}		
			finally
			{
				if (scanner != null)
					scanner.close();
			}

		}
		catch(Exception e)
		{
			getLogger().info("Config file error: " + e.getMessage());
		}
		
		TeamAPrefix = TeamAPrefix.replaceAll("&", "�");
		TeamBPrefix = TeamBPrefix.replaceAll("&", "�");

		getLogger().info("Using values:");
		getLogger().info("Team A name: " + TeamAName);
		getLogger().info("Team B name: " + TeamBName);
		getLogger().info("Team A prefix: " + TeamAPrefix);
		getLogger().info("Team B prefix: " + TeamBPrefix);

		getLogger().info("Reading player data from plugins/KCSimpleTeams/players.dat...");

		try
		{
			File f = new File("plugins/KCSimpleTeams/players.dat");
			if (!f.exists())
			{
				getLogger().info("Created player data file: plugins/KCSimpleTeams/players.dat");
				f.createNewFile(); 
				try
				{
					BufferedWriter tmpout = new BufferedWriter(new FileWriter("plugins/KCSimpleTeams/players.dat",true));
					tmpout.write("TeamA:\r\n");
					tmpout.write("TeamB:\r\n");
					tmpout.close();
				}
				catch (Exception e)
				{
					getLogger().info("Player file error: " + e.getMessage());
				}
			}
		}
		catch (Exception e)
		{
			getLogger().info("Player file error: " + e.getMessage());
		}

		this.getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			public void run() {
				Player[] playerList = Bukkit.getServer().getOnlinePlayers();
				for (int i = 0; i < playerList.length; i++)
				{
					if (NeutralPlrs.contains(playerList[i].getName()))
					{
						playerList[i].sendMessage(ChatColor.GRAY + "You have not chosen a team yet! Please pick a team with /chooseteam <A/B>");
						playerList[i].sendMessage(ChatColor.GRAY + "A: " + TeamAName);
						playerList[i].sendMessage(ChatColor.GRAY + "B: " + TeamBName);
					}
				}
			}
		}, 60L, 20 * 60L);

		reloadPlayers();
		getLogger().info("KCSimpleTeams by Jishaxe has loaded successfully.");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("team"))
		{
			if (sender instanceof Player)
			{
				if (args.length > 0)
				{
					if (TeamAPlrs.contains(sender.getName()) || TeamBPlrs.contains(sender.getName()))
					{
						sender.sendMessage(ChatColor.RED + "You are already in a team!");
						return true;
					}
					
					boolean isTeam = false;
					if (args[0].equalsIgnoreCase("a"))
					{
						isTeam = true;
						addPlayerToTeam(Team.A,sender.getName());
						sender.sendMessage(ChatColor.GREEN + "You successfully joined the team " + ChatColor.GRAY + TeamAName + ChatColor.GREEN + "!");
						((Player) sender).setDisplayName(TeamAPrefix + ((Player) sender).getDisplayName());
						
						Player[] players = Bukkit.getServer().getOnlinePlayers();
						
						for (int i = 0; i < players.length; i++)
						{
							if (players[i] != sender)
							{
								players[i].sendMessage(ChatColor.GREEN + sender.getName() + " successfully joined the team " + ChatColor.GRAY + TeamAName + ChatColor.GREEN + "!");
							}
						}
					}
					
					
					if (args[0].equalsIgnoreCase("b"))
					{
						isTeam = true;
						addPlayerToTeam(Team.B,sender.getName());
						sender.sendMessage(ChatColor.GREEN + "You successfully joined team " + ChatColor.GRAY + TeamBName + ChatColor.GREEN + "!");
						((Player) sender).setDisplayName(TeamBPrefix + ((Player) sender).getDisplayName());
						
						Player[] players = Bukkit.getServer().getOnlinePlayers();
						
						for (int i = 0; i < players.length; i++)
						{
							if (players[i] != sender)
							{
								players[i].sendMessage(ChatColor.GREEN + sender.getName() + " successfully joined the team " + ChatColor.GRAY + TeamAName + ChatColor.GREEN + "!");
							}
						}
					}
					
					if (!isTeam)
					{
						sender.sendMessage(ChatColor.RED + "You may pick either team A (" + ChatColor.GRAY + TeamAName + ChatColor.RED + " or team B (" + ChatColor.GRAY + TeamBName + ChatColor.RED + ")");
					}
					
					return true;
				}
				else
				{
					sender.sendMessage(ChatColor.RED + "You are missing the team name.");
				}
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "You must use KCSimpleTeams as an in-game player.");
			}
		}
		return false; 
	}

	public enum Team
	{
		A, B;
	}

	public void addPlayerToTeam(Team team,String name)
	{
		NeutralPlrs.remove(name);

		if (team == Team.A)
		{
			TeamAPlrs.add(name);
		}

		if (team == Team.B)
		{
			TeamBPlrs.add(name);
		}

		try
		{
			BufferedWriter tmpout = new BufferedWriter(new FileWriter("plugins/KCSimpleTeams/players.dat"));
			tmpout.write("TeamA:\r\n");
			for (int i = 0; i < TeamAPlrs.size(); i++)
			{
				tmpout.write(TeamAPlrs.get(i) + "\r\n");
			}
			tmpout.write("TeamB:\r\n");
			for (int i = 0; i < TeamBPlrs.size(); i++)
			{
				tmpout.write(TeamBPlrs.get(i) + "\r\n");
			}
			tmpout.close();
		}
		catch (Exception e)
		{
			getLogger().info("Player file error: " + e.getMessage());
		}
	}

	public void reloadPlayers()
	{
		TeamAPlrs.clear();
		TeamBPlrs.clear();
		NeutralPlrs.clear();

		File file = new File("plugins/KCSimpleTeams/players.dat"); 
		Scanner scanner = null;
		String strLine;

		try 
		{
			Boolean ABlock = false;
			Boolean BBlock = false;
			scanner = new Scanner(file);
			while (scanner.hasNextLine())
			{
				strLine = scanner.nextLine();
				if (strLine.contains("TeamA:"))
				{
					ABlock = true;
					BBlock = false;
				}

				if (strLine.contains("TeamB:"))
				{
					BBlock = true;
					ABlock = false;
				}

				if (ABlock)
				{
					if (!strLine.contains("TeamA"))
						TeamAPlrs.add(strLine);
				}

				if (BBlock)
				{
					if (!strLine.contains("TeamB"))
						TeamBPlrs.add(strLine);
				}
			}
		}
		catch (Exception e)
		{
			getLogger().info("Config file error: " + e.getMessage());
		}		
		finally
		{
			if (scanner != null)
				scanner.close();
		}

		Player[] plrlist = Bukkit.getServer().getOnlinePlayers();

		for (int i = 0; i < plrlist.length; i++)
		{
			Boolean isInTeam = false;

			for (int i2 = 0; i2 < TeamAPlrs.size(); i2++)
			{
				if (TeamAPlrs.get(i2).equalsIgnoreCase(plrlist[i].getName()))
					isInTeam = true;
			}

			for (int i2 = 0; i2 < TeamBPlrs.size(); i2++)
			{
				if (TeamBPlrs.get(i2).equalsIgnoreCase(plrlist[i].getName()))
					isInTeam = true;

			}

			if (!isInTeam)
			{
				NeutralPlrs.add(plrlist[i].getName());
			}
		}

	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		PlayerJoinEvent evt = event;
		
		boolean isInTeam = false;

		reloadPlayers();

		for (int i = 0; i < TeamAPlrs.size(); i++)
		{
			if (getServer().getPlayer(TeamAPlrs.get(i)) != null)
			{
				if (getServer().getPlayer(TeamAPlrs.get(i)).getName() == evt.getPlayer().getName())
				{
					isInTeam = true;
					evt.getPlayer().sendMessage(ChatColor.GRAY + "Welcome! You are on team " + TeamAName + "!");
					evt.getPlayer().setDisplayName(TeamAPrefix + evt.getPlayer().getDisplayName());
				}
			}

		}	

		for (int i = 0; i < TeamBPlrs.size(); i++)
		{

			if (getServer().getPlayer(TeamBPlrs.get(i)) != null)
			{
				if (getServer().getPlayer(TeamBPlrs.get(i)) == evt.getPlayer())
				{
					isInTeam = true;
					evt.getPlayer().sendMessage(ChatColor.GRAY + "Welcome! You are on team " + TeamBName + "!");
					evt.getPlayer().setDisplayName(TeamBPrefix + evt.getPlayer().getDisplayName());

				}
			}

		}	
		
		if (!isInTeam)
		{
			evt.getPlayer().sendMessage(ChatColor.GRAY + "You are not in a team. Choose a team with /chooseteam <A/B>");
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		reloadPlayers();
	}

	public void onDisable()
	{
		getLogger().info("KCSimpleTeams unloaded successfully");
	}

}
