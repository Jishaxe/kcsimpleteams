#KCSimpleTeams
##A plugin written in Java for the Minecraft server mod, Bukkit, for the KingdomCraft Minecraft server.

###Specifications:

KCSimpleTeams is a very simple team manager. Every time a player joins, they will be asked to join a team using /chooseteam.
There are two teams, Team A and Team B. When a player joins a team, they will get a prefix unique to the team they joined.
The team names can be defined in the config file located in `plugin/KCSimpleTeams/config.dat`. This is the format of the config file:
  
    <Team A name>
	<Team B name>
	<Team A prefix>
	<Team B prefix>
	
The config file is extremely simple. Here is an example (also the default):

    Red
	Blue
	&4[R]
	&1[B]

The players in each team are saved to file in a simiarly simple format, saved here: `plugins/KCSimpleTeams/players.dat`

    TeamA:
	<plr>
	<plr>
	<plr>
	TeamB:
	<plr>
	<plr>
	<plr>
	
This way, you can easily force who is in each team.

####Jishaxe Productions